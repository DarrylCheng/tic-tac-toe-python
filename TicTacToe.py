import os
from random import randint

class TicTacToe:
	def __init__ (self,playerX,playerO):
		self.players = [playerX,playerO]
		self.board = ['_' for i in range(9)]

	def dispBoard (self):
		count = 0;
		while count < 9:
			print "\t ",
			for i in range(3):
				print self.board[count],
				count += 1
			print

	def placeOnBoard(self,index,turn):
		self.board[index-1] = self.players[turn].symbol

	def checkWinning(self,board):
		conditions = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9],[3,5,7]]
		for conds in conditions:
			if(board[conds[0]-1] == '_'):
				continue
			if (board[conds[0]-1] == board[conds[1]-1] == board[conds[2]-1]):
				return True
		return False

	def boardFull(self):
		for i in self.board:
			if i == '_':
				return False
		return True

	def minimax(self,turn,board,depth,ABDict):
		if self.checkWinning(board):
			if self.players[turn].computer:
				return depth - 20 #MIN
			else:
				return 20 - depth #MAX
		score = None
		move = False
		turn = (turn + 1) % 2
		MIN = None
		MAX = None
		for i in range(9):
			if (board[i] == '_'):
				move = True
				board[i] = self.players[turn].symbol
				val = self.minimax(turn,board,depth+1,ABDict)
				board[i] = '_'
				if self.players[turn].computer:
					if score == None:
						score = val
						MAX = score
					else:
						score = min(score,val)
						MAX = max(MAX,val)
					if(ABDict['alpha'] != None and score < ABDict['alpha']):
						return score
				else:
					if score == None:
						score = val
						MIN = score
					else:
						score = max(score,val)
						MIN = min(MIN,val)
					if(ABDict['beta'] != None and score > ABDict['beta']):
						return score

		if move == False:
			return 0
		ABDict['alpha'] = MAX
		ABDict['beta'] = MIN
		return score

	def computerMove(self,turn):
		AlphaBeta = {'alpha' : None, 'beta' : None}
		MIN = None
		indices = None
		for i in range(9):
			if self.board[i] == '_':
				self.board[i] = self.players[turn].symbol
				val = self.minimax(turn,self.board,0,AlphaBeta)
				self.board[i] = '_'
				if MIN == None:
					MIN = val
					indices = [int(i)]
				elif (MIN > val):
					MIN = val
					indices = [int(i)]
				elif (MIN == val):
					indices.append(int(i))
		index = indices[randint(0,len(indices)-1)]
		self.board[index] = self.players[turn].symbol

	def suggestedMoves(self,turn):
		AlphaBeta = {'alpha' : None, 'beta' : None}
		MAX = None
		isDraw = True
		indices = None
		for i in range(9):
			if self.board[i] == '_':
				self.board[i] = self.players[turn].symbol
				val = self.minimax(turn,self.board,0,AlphaBeta)
				self.board[i] = '_'
				if val != 0:
					isDraw = False
				if MAX == None:
					MAX = val
					indices = [int(i+1)]
				elif (MAX < val):
					MAX = val
					indices = [int(i+1)]
				elif (MAX == val):
					indices.append(int(i+1))
		if isDraw:
			print "Suggested Moves -> No advantageous moves"
		elif MAX == -19:
			print "Uhh...."
		else:
			print "Suggested Moves ->", indices

class Player:
	def __init__ (self, symbol, computer):
		self.symbol = symbol
		self.computer = computer

def getInput():
	try:
		index = int(raw_input("Enter 1-9 : "))
		if not (index > 0 and index < 10):
			raise ValueError
		if Game.board[index-1] != '_':
			raise ValueError
		return index
	except NameError:
		print "Integers only!"
	except ValueError:
		print "Input out of range or place already occupied."
	return getInput()

def gameManager():
	turn = 0
	sampleBoard = [[1,2,3],[4,5,6],[7,8,9]]
	while Game.boardFull() == False:
		os.system("cls")
		print "Input Fields"
		for i in sampleBoard:
			print "\t", i
		print "\nGame board"
		Game.dispBoard()
		if Game.players[turn].computer:
			Game.computerMove(turn)
		else:
			Game.suggestedMoves(turn)
			index = getInput()
			Game.placeOnBoard(index,turn)
		if Game.checkWinning(Game.board):
			break
		turn = (turn + 1) % 2
	os.system("cls")
	print "Game board"
	Game.dispBoard()
	if Game.checkWinning(Game.board):
 		print "The winner is " + Game.players[turn].symbol
 	else:
 		print "Draw game!"

def MainMenu(players):
	while True:
		os.system("cls")
		print "\tTic Tac Toe\n\n";
		print "\t1. Play with Computer with hints (You first)\n";
		print "\t2. Play with Computer with hints (Computer first)\n";
		print "\n\t3. Quit\n\n";
		index = int(raw_input("==> "))
		if not (index > 0 and index < 5):
			continue
		if index == 3:
			raise SystemExit(0)
		elif index == 1:
			players.append(Player('X',False))
			players.append(Player('O',True))
		elif index == 2:
			players.append(Player('X',True))
			players.append(Player('O',False))
		return

players = []
MainMenu(players)
Game = TicTacToe(players[0],players[1])
gameManager()